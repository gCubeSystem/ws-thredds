This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.spatial.data.ws-thredds

## [v1.0.1] 

Security Fixes
Fixes [#21783]

## [v1.0.0] 
Integration with new IAM
Security Fixes
Fixes [#21537]


## [v0.2.5]
Fixes #21265


## [v0.2.4]
Default validate flag= false
From http to https

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)
