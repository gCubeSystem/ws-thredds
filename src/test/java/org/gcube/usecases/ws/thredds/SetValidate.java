package org.gcube.usecases.ws.thredds;

import java.util.Map;

import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.model.Metadata;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.usecases.ws.thredds.engine.impl.WorkspaceFolderManager;
import org.gcube.usecases.ws.thredds.faults.WorkspaceInteractionException;

public class SetValidate {

	public static void main(String[] args) throws WorkspaceInteractionException, StorageHubException {
		String itemId="e4a0f901-5d8e-4b44-a947-4105d9b04a55";
		String context="/pred4s/preprod/preVRE";
		Boolean toSetValidate=false;
		
		
		
		TokenSetter.set(context);
		
		WorkspaceFolderManager mng=new WorkspaceFolderManager(itemId);
		
		FolderContainer folder=mng.getTheFolder();
		Metadata meta=folder.get().getMetadata();
		Map<String,Object> map=meta.getMap();
		
		map.put(Constants.WorkspaceProperties.VALIDATE_METADATA,toSetValidate+"");
		
		
		meta.setMap(map);
		folder.setMetadata(meta);
		System.out.println("DONE");
	}

}
