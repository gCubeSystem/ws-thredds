package org.gcube.usecases.ws.thredds;

import java.util.Date;

import org.gcube.usecases.ws.thredds.engine.impl.WorkspaceUtils;

public class WorkspaceAccounting {

	public static void main(String[] args) throws Exception {
		TestCommons.setScope();
		
		
		
//		WorkspaceFolder folder=ws.getRoot().createFolder("Accounting", "test purposes");
//		
//		
//		WorkspaceFolder subFolder=folder.createFolder("SubFolder", "Will be removed");
//
//		
//		ExternalFile toBeRemovedFile=folder.createExternalFileItem("The file", "file to be removed", "application/xml", File.createTempFile("tmp", ".tmp"));
//		
//		subFolder.remove();
//		toBeRemovedFile.remove();
//		
//		
//		for(AccountingEntry entry:folder.getAccounting()) {
//			try {
//				
//				
//				Date eventTime=entry.getDate().getTime();
//				String toDeleteRemote=null;
//				switch(entry.getEntryType()) {
//				case REMOVAL:{					
//					AccountingEntryRemoval removalEntry=(AccountingEntryRemoval) entry;
//					System.out.println(removalEntry.getItemName() +"REMOVED. FolderItemType "+removalEntry.getFolderItemType()+" ItemType "+removalEntry.getItemType());
//					
//					
//					break;
//				}
//				case RENAMING:{					
//					AccountingEntryRenaming renamingEntry=(AccountingEntryRenaming) entry;					
//					toDeleteRemote=renamingEntry.getOldItemName();				
//					
//				}
//				case CUT:{					
//					AccountingEntryCut cut = (AccountingEntryCut) entry;
//					toDeleteRemote=cut.getItemName();
//					break;
//				}
//				}
//				
//			}catch(Throwable t) {
//				t.printStackTrace();
//			}
//		}
//
//	}

		
		
		System.out.println(WorkspaceUtils.isModifiedAfter(TestCommons.getByPath(TestCommons.getTestFolder().get().getPath()+"/mySub/dissolved_oxygen_annual_5deg_ENVIRONMENT_BIOTA_.nc").get(), new Date(0l)));
		
		
	}
}
