package org.gcube.usecases.ws.thredds;

import org.gcube.data.transfer.library.utils.ScopeUtils;
import org.gcube.data.transfer.model.plugins.thredds.DataSetScan;
import org.gcube.data.transfer.model.plugins.thredds.ThreddsInfo;
import org.gcube.usecases.ws.thredds.engine.impl.ThreddsController;
import org.gcube.usecases.ws.thredds.faults.InternalException;

public class ThreddsCatalogTests {

	public static void main(String[] args) throws InternalException {
		TokenSetter.set("/gcube/devsec");

		System.out.println("Current scope is "+ScopeUtils.getCurrentScope());

		//		SyncEngine engine=SyncEngine.get();
		//		
		//		String[] toCheckTokens=new String[] {
		//			"..."
		//		};
		//		
		//		
		//		for(String token:toCheckTokens) {
		//			System.out.println("Checking catalog for token "+token);
		//			for(CatalogBean bean:engine.getAvailableCatalogsByToken(token)) 
		//				System.out.println(bean.getName()+" in "+bean.getPath()+" Default : "+bean.getIsDefault());
		//			
		//			System.out.println("************************************");
		//			System.out.println();
		//		}
		//		


		ThreddsController controller=new ThreddsController("");

		ThreddsInfo info=controller.getThreddsInfo();
		String [] paths=new String[] {
				"/data/content/thredds/newer",
				"/data/content/thredds/public",
				"/data/content/thredds/public/netcdf",
				"/data/content/thredds/public/netcdf/GPTest",
		};

		for(String path:paths) {
			if(info.getCatalogByFittingLocation(path)!=null) {
				DataSetScan ds=(DataSetScan) info.getDataSetFromLocation(path);
				if(ds==null)
					System.out.println("Catalog for "+path+"\t is null.");
				else System.out.println("Catalog for "+path+"\t : "+ds.getLocation()+"\t "+ds.getName());
			}else System.out.println("No catalog for path "+path);
		}

	}

}
