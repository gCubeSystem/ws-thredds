package org.gcube.usecases.ws.thredds;

import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.model.Metadata;

public class WorkspaceCleanup {

	public static void main(String[] args) throws Exception{
		TestCommons.setScope();
		FolderContainer folder=TestCommons.getTestFolder();
		
		Metadata meta=folder.get().getMetadata();
		meta.setMap(Constants.cleanedFolderPropertiesMap);
		folder.setMetadata(meta);
		
		SyncEngine engine=SyncEngine.get();
		engine.forceUnlock(folder.getId());
//		engine.unsetSynchronizedFolder(folder.getId(), false);
		
//		
		WorkspaceProperties.printProperties(folder);
	}

}
