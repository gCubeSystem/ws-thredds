package org.gcube.usecases.ws.thredds;

import org.gcube.data.transfer.model.plugins.thredds.ThreddsInfo;
import org.gcube.usecases.ws.thredds.engine.impl.ThreddsController;
import org.gcube.usecases.ws.thredds.model.SynchFolderConfiguration;

public class ThreddsTests {

	public static void main(String[] args) throws Exception{
		TestCommons.setScope();
		
		SynchFolderConfiguration folderConfig=TestCommons.getSynchConfig();
		ThreddsController controller=TestCommons.getThreddsController();
		
		System.out.println("Getting thredds info...");
		
		ThreddsInfo info=controller.getThreddsInfo();
		
		System.out.println("INFO "+info);
		
		System.out.println(info.getCatalogByFittingLocation(info.getLocalBasePath()+"/"+folderConfig.getRemotePath()));
	}

}
