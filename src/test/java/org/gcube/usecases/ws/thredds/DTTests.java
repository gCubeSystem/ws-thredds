package org.gcube.usecases.ws.thredds;

import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.usecases.ws.thredds.engine.impl.WorkspaceUtils;
import org.gcube.usecases.ws.thredds.engine.impl.security.Security;
import org.gcube.usecases.ws.thredds.model.SynchFolderConfiguration;

public class DTTests {

	public static void main(String[] args) throws Exception {
		
		TokenSetter.set("/gcube/devNext");
//		Workspace ws = HomeLibrary.getHomeManagerFactory().getHomeManager().getHome().getWorkspace();
		StorageHubClient client=WorkspaceUtils.getClient();
		
		String folderId=TestCommons.getByPath("/Workspace/Accounting").getId();
		SyncEngine.get().setSynchronizedFolder(new SynchFolderConfiguration("another", "", Security.getCurrent().getContext(), "dummy",folderId), folderId);
		System.out.println("Done");
		
	}

}
