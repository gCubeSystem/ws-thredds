package org.gcube.usecases.ws.thredds;

import java.util.HashMap;
import java.util.Map;

import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.ItemContainer;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.usecases.ws.thredds.engine.impl.ThreddsController;
import org.gcube.usecases.ws.thredds.engine.impl.WorkspaceUtils;
import org.gcube.usecases.ws.thredds.engine.impl.security.Security;
import org.gcube.usecases.ws.thredds.model.SynchFolderConfiguration;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class TestCommons {

	@Getter
	@AllArgsConstructor
	static class TestSet{
		String label;
		String scope;
		String folderId;
		String remotePath;
		String targetToken;
		String toCreateCatalogName;
		
		
		public String getFolderId() throws Exception {
			if(folderId.startsWith("/"))
				return getByPath(folderId).getId();
			else return folderId;
		}
		
	}
	
	private static Map<String,TestSet> configs=new HashMap<>();
	
	private static String toUseConfig="simple";
	
	
	static {
		
		
		configs.put("simple", new TestSet("Simple label ","/gcube/devsec/devVRE", "Test1","public/netcdf/simpleFolder","3aa85bbf-d5f3-4df4-ad03-4f8f788eba3d-98187548","simple"));
		
		configs.put("root", new TestSet("Simple label ","/gcube", "Test2","public/netcdf/wrong","3aa85bbf-d5f3-4df4-ad03-4f8f788eba3d-98187548","Wrong"));

		
	}
	
	
	public static ItemContainer<?> getByPath(String path) throws Exception  {
		StorageHubClient client = WorkspaceUtils.getClient();
		FolderContainer root=client.getWSRoot();
		
		return scan(root,path);
	}
	
	
	public static ItemContainer<?> scan(FolderContainer folder,String path) throws Exception{
		String toLookFor=path.substring((path.startsWith("/")?1:0), path.length());
		String[] split=toLookFor.split("/");
		toLookFor=split[0];
		for(ItemContainer<?> item:folder.list().withMetadata().getContainers())
				if(item.get().getName().equals(toLookFor)) {
					if(split.length>1) return scan((FolderContainer)item,path.substring(toLookFor.length()));
					else return item;
				}
		throw new Exception("Unable to find "+path);
	}
	
	
	public static void setScope() {		
		TokenSetter.set(configs.get(toUseConfig).getScope());
	}
	
	
	public static FolderContainer getTestFolder() throws Exception {
		
		try{
			return WorkspaceUtils.getClient().open(configs.get(toUseConfig).getFolderId()).asFolder();
		}catch(Throwable t) {
			// try to use path
			return (FolderContainer) getByPath(configs.get(toUseConfig).getFolderId());
		}
		
		
		
//		WorkspaceFolder folder=null;
//		try{
//			folder=ws.getRoot().createFolder(folderName+"2", "test purposes");
//		}catch(ClassCastException e) {
//			folder=(WorkspaceFolder) ws.getItemByPath("/Workspace/"+folderName+"2");
//		}
//		
//		String datasetUrl="https://thredds-d-d4s.d4science.org/thredds/fileServer/public/netcdf/test%20by%20Francesco/dissolved_oxygen_annual_5deg_ENVIRONMENT_BIOTA_.nc";
//		try {
//			folder.createExternalFileItem("dissolved_oxygen_annual_5deg_ENVIRONMENT_BIOTA_.nc", "nc test file", "application/x-netcdf", new URL(datasetUrl).openStream());
//		}catch(Exception e) {
//			// file already existing..
//		}
//		return folder;
	}
	
	
	public static ThreddsController getThreddsController() throws Exception {
		SynchFolderConfiguration config=getSynchConfig();
		return new ThreddsController(config.getRemotePath());
	}
	
	public static SynchFolderConfiguration getSynchConfig() throws Exception {
		TestSet set=configs.get(toUseConfig);		
		return new SynchFolderConfiguration(set.getRemotePath(), "*.nc,*.ncml,*.asc", Security.getCurrent().getContext(),set.getToCreateCatalogName(),set.getFolderId());
	}
}
