package org.gcube.usecases.ws.thredds;

import org.gcube.common.storagehub.client.dsl.ContainerType;
import org.gcube.common.storagehub.client.dsl.FileContainer;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.ItemContainer;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.usecases.ws.thredds.model.SynchFolderConfiguration;

public class GetWSSynchSpace {

	public static void main(String[] args) throws Exception {
		TokenSetter.set("/gcube");
		SyncEngine engine=SyncEngine.get();

		FolderContainer folder=(FolderContainer) TestCommons.getByPath("ThreddsDev");
		
		System.out.println(folder.getId());
		
		SynchFolderConfiguration config=new SynchFolderConfiguration();
		System.out.println("Total size : "+computeLength(folder, config));
	}

	
	private static final long computeLength(ItemContainer<?> item,SynchFolderConfiguration config) throws StorageHubException{
		long toReturn=0l;
		if(item.getType().equals(ContainerType.FOLDER)) {
			for(ItemContainer<?> child:((FolderContainer)item).list().withContent().getContainers()) {
				if(child.getType().equals(ContainerType.FOLDER)||config.matchesFilter(child.get().getName()))
						toReturn=toReturn+computeLength(child, config);
			}
		}else toReturn=toReturn+((FileContainer)item).get().getContent().getSize();
		return toReturn;
	}
	
	
}
