package org.gcube.usecases.ws.thredds;

import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.ItemContainer;
import org.gcube.usecases.ws.thredds.engine.impl.WorkspaceUtils;

public class WSTimes {

	public static void main(String[] args) throws Exception {
		TestCommons.setScope();
		FolderContainer folder=TestCommons.getTestFolder();
		for(ItemContainer<?> item : folder.list().withMetadata().getContainers()) {
			printDates(item);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
			
			}
			WorkspaceUtils.addParameter(item,"Fake prop", "fake value");
			printDates(item);
		}
	}

	
	public static final void printDates(ItemContainer<?> item) {
		System.out.println("ITEM : "+item.get().getName());
		System.out.println("Creation Date : "+Constants.DATE_FORMAT.format(item.get().getCreationTime().getTime()));
		System.out.println("Creation Date : "+Constants.DATE_FORMAT.format(item.get().getLastModificationTime().getTime()));
	}
	
}
