package org.gcube.usecases.ws.thredds;

import java.io.File;
import java.util.Map;
import java.util.Map.Entry;

import org.gcube.common.storagehub.client.dsl.ContainerType;
import org.gcube.common.storagehub.client.dsl.FileContainer;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.ItemContainer;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.data.transfer.library.DataTransferClient;
import org.gcube.data.transfer.model.Destination;
import org.gcube.data.transfer.model.DestinationClashPolicy;
import org.gcube.spatial.data.sdi.interfaces.Metadata;
import org.gcube.spatial.data.sdi.model.metadata.MetadataPublishOptions;
import org.gcube.spatial.data.sdi.model.metadata.MetadataReport;
import org.gcube.spatial.data.sdi.model.metadata.TemplateInvocationBuilder;
import org.gcube.spatial.data.sdi.plugins.SDIAbstractPlugin;
import org.gcube.usecases.ws.thredds.engine.impl.WorkspaceUtils;
import org.gcube.usecases.ws.thredds.model.SynchFolderConfiguration;

public class PublicLinkIssueTest {

	
	public static void main (String[] args) throws Exception {
		TokenSetter.set("/d4science.research-infrastructures.eu");
		
		StorageHubClient shClient=WorkspaceUtils.getClient();
		System.out.println(((FileContainer)TestCommons.getByPath("ArgoNetCDF/Practical_salinity/Practical_salinity_code_30_date_2000_6.nc")).getPublicLink());
		
		String threddsHostName = Commons.getThreddsHost();

		DataTransferClient client=Commons.getDTClient(threddsHostName);
		Destination dest=new Destination("thredds", "public/netcdf/someWhere", "myTest.txt", true, DestinationClashPolicy.REWRITE, DestinationClashPolicy.APPEND);
		
		
		
		
		
		client.httpSource("http://data.d4science.org/V2drR2gxSFRTQlpLVC9nakozL29QcDdPR2U5UEVHYWRHbWJQNStIS0N6Yz0", dest);
		
		
		scanForPrint(shClient.open("a8cd78d3-69e8-4d02-ac90-681b2d16d84d").asFolder());
		System.out.println("OK FIRST ...");
		try {
		Metadata meta=SDIAbstractPlugin.metadata().build();

		

		MetadataPublishOptions opts=new MetadataPublishOptions(new TemplateInvocationBuilder().threddsOnlineResources(threddsHostName, "myMeta", "testCatalog").get());
		opts.setGeonetworkCategory("Datasets");
		MetadataReport report=meta.pushMetadata(new File("/home/fabio/Desktop/meta.xml"), opts);
		}catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println("CHECKING AGAIN");
		scanForPrint(shClient.open("a8cd78d3-69e8-4d02-ac90-681b2d16d84d").asFolder());
		
		
	}
	
	public static void scanForPrint(FolderContainer folder) throws StorageHubException {
		System.out.println("Folder "+folder.get().getPath());
		printProperties(folder.get());
		SynchFolderConfiguration config=new SynchFolderConfiguration("", "", "", "","");
		for(ItemContainer<?> item:folder.list().withMetadata().getContainers())
			if(!item.getType().equals(ContainerType.FOLDER)&&config.matchesFilter(item.get().getName())) {
//				System.out.println("ITEM "+item.getPath());
				printProperties(item.get());
			}
		for(ItemContainer<?> item:folder.list().withMetadata().getContainers())
			if(item.getType().equals(ContainerType.FOLDER))scanForPrint((FolderContainer) item);
	}
	
	
	public static void printProperties(Item toPrint){
		Map<String,Object> map=toPrint.getMetadata().getMap();
//		System.out.print("Properties : ..");
		for(Entry<String,Object> entry:map.entrySet()) {
			if(entry.getKey().equals(Constants.WorkspaceProperties.SYNCHRONIZATION_STATUS)||
					entry.getKey().equals(Constants.WorkspaceProperties.LAST_UPDATE_STATUS)||
					entry.getKey().equals(Constants.WorkspaceProperties.LAST_UPDATE_TIME)) {
//			if(true) {
				if(entry.getValue()==null) System.out.print(entry.getKey()+" is null;");
				else System.out.print(entry.getKey()+" = "+entry.getValue()+";");
			}
		}
//		System.out.println();
	}
	
	
}
