package org.gcube.usecases.ws.thredds;

import java.util.Collections;

import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.usecases.ws.thredds.engine.impl.WorkspaceFolderManager;
import org.gcube.usecases.ws.thredds.engine.impl.WorkspaceUtils;

public class WorkspaceLock {

	public static void main(String[] args) throws Exception  {
		TestCommons.setScope();
		FolderContainer folder=TestCommons.getTestFolder();
		WorkspaceFolderManager manager=new WorkspaceFolderManager(folder.getId());
		
		String processID="mytest";
		try {
			System.out.println("Trying to cleanup, first.. ");
			WorkspaceUtils.addParameters(folder, Collections.singletonMap(org.gcube.usecases.ws.thredds.Constants.WorkspaceProperties.TBS, null));
			System.out.println("FOLDER PROPERTIES : "+folder.get().getMetadata().getMap());
		}catch(Throwable t) {
			System.err.println("Dismiss error : ");
			t.printStackTrace();
		}
		
		
		
//		manager.configure(new SynchFolderConfiguration("mySynchedCatalog","*.nc,*.ncml,*.asc",SecurityTokenProvider.instance.get()));
		
		System.out.println("Is locked : "+manager.isLocked());
		System.out.println("locking ... ");
		manager.lock(processID);
		
		try {
			manager.dismiss(false);
			System.err.println("It should have raised locked exception");
		}catch(Exception e){
			System.out.println("Ok check lock on dismiss ");
		}
			
		manager.unlock(processID);
		
		manager.dismiss(false);

		
		System.out.println("This should be false : "+manager.isSynched());
	}

}
