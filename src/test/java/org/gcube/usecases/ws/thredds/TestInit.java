package org.gcube.usecases.ws.thredds;

import org.gcube.spatial.data.sdi.utils.ScopeUtils;
import org.gcube.usecases.ws.thredds.faults.InternalException;

public class TestInit {

	public static void main(String[] args) throws InternalException {
		TokenSetter.set("/gcube/devsec/devVRE");
		
		System.out.println("Context "+ScopeUtils.getCurrentScope()+" token "+ScopeUtils.getCurrentCaller());
		final SyncEngine engine=SyncEngine.get();
		
		
//		// TO BE INTRODUCED LATER 
//		engine.getAvailableCatalogsByToken(Security.getToken()).forEach((CatalogBean c)->{
//			
//			System.out.println(c);
//			
//		});
		
	}
	
}
