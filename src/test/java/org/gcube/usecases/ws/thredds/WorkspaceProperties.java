package org.gcube.usecases.ws.thredds;

import java.util.Map;
import java.util.Map.Entry;

import org.gcube.common.storagehub.client.dsl.ContainerType;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.ItemContainer;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.usecases.ws.thredds.engine.impl.WorkspaceFolderManager;
import org.gcube.usecases.ws.thredds.faults.WorkspaceLockedException;
import org.gcube.usecases.ws.thredds.faults.WorkspaceNotSynchedException;
import org.gcube.usecases.ws.thredds.model.SynchFolderConfiguration;

public class WorkspaceProperties {

	public static void main(String[] args) throws Exception{
		
		TestCommons.setScope();
		FolderContainer folder=TestCommons.getTestFolder();
//		System.out.println("Props : "+folder.get().getMetadata().getMap().size());
//		printProperties(folder);
//		System.out.println(new WorkspaceFolderManager(folder.getId()).isSynched());
		
		try{
			SyncEngine.get().check(folder.getId(), true);
		}catch(WorkspaceLockedException e) {
			System.err.println("Workspace is locked.");
		}
		scanForPrint(folder);
		
		
//		SyncEngine.get().shutDown();
//		for(Workspace)
//		printProperties(folder.getProperties());
//		
//		System.out.println("Has property : "+folder.getProperties().hasProperty(org.gcube.usecases.ws.thredds.Constants.WorkspaceProperties.TBS));
//		printProperties(folder.getProperties());
//		
//		
//		System.out.println("Setting property.. ");
//		folder.getProperties().addProperties(Collections.singletonMap(org.gcube.usecases.ws.thredds.Constants.WorkspaceProperties.TBS, "true"));
//		
//		System.out.println("Has property : "+folder.getProperties().hasProperty(org.gcube.usecases.ws.thredds.Constants.WorkspaceProperties.TBS));
//		printProperties(folder.getProperties());
//		
//		
//		System.out.println("Removing (setting it null) ");
//		folder.getProperties().addProperties(Collections.singletonMap(org.gcube.usecases.ws.thredds.Constants.WorkspaceProperties.TBS, null));
//		
//		System.out.println("Has property : "+folder.getProperties().hasProperty(org.gcube.usecases.ws.thredds.Constants.WorkspaceProperties.TBS));
//		printProperties(folder.getProperties());
//		
//		String folderId=
//		
//		WorkspaceFolderManager manager=new WorkspaceFolderManager(folderId);
//		
//		manager.configure(new SynchFolderConfiguration("myRemoteFolder","thredds","*.nc,*.ncml,*.asc"));
//		
//		manager.dismiss(false);
		
	}

	public static void scanForPrint(FolderContainer folder) throws StorageHubException {
		System.out.println("Folder "+folder.get().getPath());
		printProperties(folder);
		SynchFolderConfiguration config=new SynchFolderConfiguration("", "", "", "","");
		for(ItemContainer<?> item:folder.list().withMetadata().getContainers())
			if(!item.getType().equals(ContainerType.FOLDER)&&config.matchesFilter(item.get().getName())) {
				System.out.println("ITEM "+item.get().getPath());
				printProperties(item);
			}
		for(ItemContainer<?> item:folder.list().withMetadata().getContainers())
			if(item.getType().equals(ContainerType.FOLDER))scanForPrint((FolderContainer) item);
		
	}
	
	
	public static void printProperties(ItemContainer<?> item) {
		Map<String,Object> map=item.get().getMetadata().getMap();
		System.out.print("Properties : ..");
		for(Entry<String,Object> entry:map.entrySet()) {
//			if(entry.getKey().equals(Constants.WorkspaceProperties.SYNCHRONIZATION_STATUS)||
//					entry.getKey().equals(Constants.WorkspaceProperties.LAST_UPDATE_STATUS)||
//					entry.getKey().equals(Constants.WorkspaceProperties.LAST_UPDATE_TIME)) {
			if(true) {
				if(entry.getValue()==null) System.out.print(entry.getKey()+" is null;");
				else System.out.print(entry.getKey()+" = "+entry.getValue()+";");
			}
			try{
				System.out.println(SyncEngine.parseInfo(map, org.gcube.usecases.ws.thredds.model.ContainerType.valueOf(item.getType().toString())));
			}catch(Throwable t) {
				System.err.println("ITEM ID "+item.getId()+t.getMessage());
			}
		}
		System.out.println();
	}
	
}
