package org.gcube.usecases.ws.thredds;

import java.util.concurrent.Semaphore;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.usecases.ws.thredds.engine.impl.ProcessDescriptor;
import org.gcube.usecases.ws.thredds.engine.impl.ProcessStatus;
import org.gcube.usecases.ws.thredds.faults.WorkspaceLockedException;
import org.gcube.usecases.ws.thredds.faults.WorkspaceNotSynchedException;
import org.gcube.usecases.ws.thredds.model.SyncOperationCallBack;
import org.gcube.usecases.ws.thredds.model.SynchFolderConfiguration;

public class WorkspaceSynchronizationTest {

	public static void main(String[] args) throws Exception {
		
		// GET ENGINE : SINGLETON INSTANCE
		final SyncEngine engine=SyncEngine.get();
		engine.setRequestLogger("requests.txt");
		
		//TEST INFO...
		TestCommons.setScope();
		System.out.println(ScopeProvider.instance.get());
		FolderContainer folder=TestCommons.getTestFolder();
		
		// FOLDER CONFIGURATION BEAN
		SynchFolderConfiguration config=TestCommons.getSynchConfig();
//		SynchFolderConfiguration config=null;
		
		
//		try {
//			//try to clean it up, first..
//			System.out.println("Cleaning it up.."); 
//			engine.unsetSynchronizedFolder(folder.getId(), false);
//		}catch(WorkspaceNotSynchedException e) {
//			// it was already cleared
//		}catch(WorkspaceLockedException e) {
//			engine.forceUnlock(folder.getId());
//			engine.unsetSynchronizedFolder(folder.getId(), false);			
//		}

		String folderId=folder.getId();
//		String folderId="15db7b0e-c215-41b7-b384-a1deda250b65";
		
		
		try {
			// WHEN OPENING A FOLDER, INVOKE CHECK TO UPDATE SYNCH STATUS 
			engine.check(folderId, false);
		}catch(WorkspaceNotSynchedException e) {
			System.out.println("Folder not synched, configurin it..");
			engine.setSynchronizedFolder(config, folderId);
			engine.check(folderId, false);
		}catch(WorkspaceLockedException e) {
			System.out.println("Workspace locked, going to force unlock.."); // MAINLY FOR TEST PURPOSES, OR WHEN SOMETHIGN GOES WRONG.. USE CAUTIOUSLY
			engine.forceUnlock(folderId);
			engine.check(folderId, false);
		}
		
		
		
		
		// INVOKE SYNCHRONIZATION ON FOLDER
		ProcessDescriptor descriptor=engine.doSync(folderId);
		
		System.out.println("Obtained descriptor : "+descriptor);
		
		Semaphore sem=new Semaphore(0);
		
		// REGISTER CALLBACK TO MONITOR PROGRESS
		engine.registerCallBack(folderId, new SyncOperationCallBack() {
			
			@Override
			public void onStep(ProcessStatus status, ProcessDescriptor descriptor) {
				System.out.println("ON STEP : "+status+" "+descriptor);
				System.out.println("ENGINE STATUS : "+engine.getStatus());
				System.out.println("LOG : \n"+ status.getLogBuilder().toString());
				if(status.getStatus().equals(ProcessStatus.Status.COMPLETED)) sem.release();
			}
		});
		
		System.out.println("Waiting for process.. ");
		try {
			sem.acquire();
		} catch (InterruptedException e) {
			
		}
		
		engine.check(folderId, true);
		
		
		// INVOKE WHEN PORTAL SHUTS DOWN TO FREE RESOURCES AND STOP SYNC PROCESSES
		engine.shutDown();
		
	}

}
