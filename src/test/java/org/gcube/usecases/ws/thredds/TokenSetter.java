package org.gcube.usecases.ws.thredds;

import java.util.Properties;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.usecases.ws.thredds.engine.impl.security.Security;
import org.gcube.usecases.ws.thredds.engine.impl.security.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TokenSetter {
	
	private static Properties props=null;
	

	public static synchronized void set(String scope){
			if(props==null) {
				props=new Properties();
				try {
					props.load(TokenSetter.class.getResourceAsStream("/tokens.properties"));
				} catch (Exception e) {
					throw new RuntimeException("YOU NEED TO SET TOKEN FILE IN CONFIGURATION");
				}
			}
			if(!props.containsKey(scope)) throw new RuntimeException("No token found for scope : "+scope);
			Security.set(new User(null, null, props.getProperty(scope), scope));
	}
	
}
