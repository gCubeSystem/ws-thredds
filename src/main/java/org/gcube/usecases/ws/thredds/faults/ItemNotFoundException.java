package org.gcube.usecases.ws.thredds.faults;

public class ItemNotFoundException extends WorkspaceInteractionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6818076224344812668L;

	public ItemNotFoundException() {
		// TODO Auto-generated constructor stub
	}

	public ItemNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ItemNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ItemNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ItemNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
