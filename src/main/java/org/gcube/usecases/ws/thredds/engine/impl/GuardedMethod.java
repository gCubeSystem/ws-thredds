package org.gcube.usecases.ws.thredds.engine.impl;

import org.gcube.usecases.ws.thredds.engine.impl.security.Security;
import org.gcube.usecases.ws.thredds.faults.InternalException;
import org.gcube.usecases.ws.thredds.faults.WorkspaceInteractionException;
import org.gcube.usecases.ws.thredds.model.SynchFolderConfiguration;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class GuardedMethod<T> {

	@Getter
	private WorkspaceFolderManager manager;
	@Getter
	private SynchFolderConfiguration configuration;
	
	@Getter
	private String folderId;
	@Getter
	private T result=null;
	
	public GuardedMethod(String folderId) throws WorkspaceInteractionException {
		this.folderId=folderId;
		manager=new WorkspaceFolderManager(folderId);
		configuration=manager.getSynchConfiguration();
		Security.checkOperator(configuration);
	}
	
	
	public GuardedMethod<T> execute() throws WorkspaceInteractionException,InternalException{
		try {			
			result=run();
			return this;
		}catch(WorkspaceInteractionException|InternalException e) {
			throw e;
		}catch(Throwable t) {
			log.error("Unexpected error ",t);
			throw new InternalException("Unexpected internal error", t);
		}
		
	}
	
	protected abstract T run() throws WorkspaceInteractionException,InternalException,Exception;

}
