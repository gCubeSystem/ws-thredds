package org.gcube.usecases.ws.thredds.model;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class SyncEngineStatusDescriptor {

	private int parallelTransferCount;
	private int queueLenght;
	private Map<String,String> configuration;

}
