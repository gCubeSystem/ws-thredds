package org.gcube.usecases.ws.thredds;

import java.io.File;
import java.util.Map;
import java.util.Set;

import org.gcube.usecases.ws.thredds.engine.impl.ProcessDescriptor;
import org.gcube.usecases.ws.thredds.engine.impl.ProcessStatus;
import org.gcube.usecases.ws.thredds.engine.impl.SynchEngineImpl;
import org.gcube.usecases.ws.thredds.engine.impl.WorkspaceUtils;
import org.gcube.usecases.ws.thredds.faults.InternalException;
import org.gcube.usecases.ws.thredds.faults.ProcessNotFoundException;
import org.gcube.usecases.ws.thredds.faults.WorkspaceInteractionException;
import org.gcube.usecases.ws.thredds.faults.WorkspaceNotSynchedException;
import org.gcube.usecases.ws.thredds.model.ContainerType;
import org.gcube.usecases.ws.thredds.model.SyncEngineStatusDescriptor;
import org.gcube.usecases.ws.thredds.model.SyncFolderDescriptor;
import org.gcube.usecases.ws.thredds.model.SyncOperationCallBack;
import org.gcube.usecases.ws.thredds.model.SynchFolderConfiguration;
import org.gcube.usecases.ws.thredds.model.SynchronizedElementInfo;
import org.gcube.usecases.ws.thredds.model.gui.CatalogBean;

public interface SyncEngine {

	public static SyncEngine get() {
		return SynchEngineImpl.get();
	}
	
	/**
	 * Checked Access Method
	 */
	public SyncFolderDescriptor check(String folderId, boolean recursively) throws WorkspaceInteractionException, InternalException;
	public void registerCallBack(String folderId,SyncOperationCallBack callback) throws ProcessNotFoundException;
	
	/**
	 * Checked Access Method
	 */
	public ProcessDescriptor doSync(String folderId) throws WorkspaceInteractionException, InternalException;
	
	/**
	 * Checked Access Method
	 */
	public void stopSynch(String folderId) throws ProcessNotFoundException, WorkspaceInteractionException, InternalException;
	
	
	/**
	 * Checked Access Method
	 */
	public void setSynchronizedFolder(SynchFolderConfiguration config,String folderId) throws WorkspaceInteractionException, InternalException;
	
	/**
	 * Checked Access Method
	 */
	public void unsetSynchronizedFolder(String folderId,boolean deleteRemoteContent) throws WorkspaceInteractionException, InternalException;
	
	/**
	 * NB Method To Be Implemented
	 * 
	 * @param elementId
	 * @return
	 */
	public SynchronizedElementInfo getInfo(String elementId);
	
	
	/**
	 * Checked Access Method
	 * @throws WorkspaceInteractionException 
	 */
	public void updateCatalogFile(String folderId, File toUpdate) throws InternalException, WorkspaceInteractionException;
	
	/**
	 * Checked Access Method 
	 * 
	 * 
	 * @param folderId
	 * @throws InternalException
	 * @throws WorkspaceInteractionException
	 */
	
	public void forceUnlock(String folderId)throws InternalException, WorkspaceInteractionException;


	public void shutDown();
	
	
	public ProcessDescriptor getProcessDescriptorByFolderId(String folderId)throws ProcessNotFoundException;
	public ProcessStatus getProcessStatusByFolderId(String folderId)throws ProcessNotFoundException;
	
	
	
	public void setRequestLogger(String path);
	public boolean isRequestLoggerEnabled();
	public String getRequestLoggerPath();
	
	
	public Set<CatalogBean> getAvailableCatalogs() throws InternalException;
		
	public SyncEngineStatusDescriptor getStatus();
	
	
	
	public SynchFolderConfiguration getConfig(String fodlerId) throws WorkspaceInteractionException, WorkspaceNotSynchedException;
	
	
	
	public static SynchronizedElementInfo parseInfo(Map<String,Object> itemProperties,ContainerType itemType)throws WorkspaceNotSynchedException{
		return WorkspaceUtils.getInfo(itemProperties, org.gcube.common.storagehub.client.dsl.ContainerType.valueOf(itemType.toString()));
	}
}
