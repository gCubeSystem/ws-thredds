package org.gcube.usecases.ws.thredds.faults;

public class WorkspaceFolderNotRootException extends WorkspaceInteractionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5025376971995804122L;

	public WorkspaceFolderNotRootException() {
		// TODO Auto-generated constructor stub
	}

	public WorkspaceFolderNotRootException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public WorkspaceFolderNotRootException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public WorkspaceFolderNotRootException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public WorkspaceFolderNotRootException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
