package org.gcube.usecases.ws.thredds.model;

import org.gcube.usecases.ws.thredds.engine.impl.ProcessDescriptor;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper=true)
public class SyncFolderDescriptor extends SynchronizedElementInfo{

	@NonNull
	private String folderId;
	@NonNull
	private String folderPath;
	@NonNull
	private SynchFolderConfiguration configuration;
	
	@NonNull
	private Boolean isLocked=false;
	
	
	
	
	
	public SyncFolderDescriptor(SynchronizationStatus status, String folderId, String folderPath,
			SynchFolderConfiguration configuration, Boolean isLocked) {
		super(status);
		this.folderId = folderId;
		this.folderPath = folderPath;
		this.configuration = configuration;
		this.isLocked = isLocked;
	}





	private ProcessDescriptor localProcessDescriptor=null; 
	
	
}
