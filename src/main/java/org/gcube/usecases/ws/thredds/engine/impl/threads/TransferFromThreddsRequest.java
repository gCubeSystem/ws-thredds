package org.gcube.usecases.ws.thredds.engine.impl.threads;

import org.gcube.common.storagehub.model.items.FolderItem;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.usecases.ws.thredds.Constants;
import org.gcube.usecases.ws.thredds.engine.impl.Process;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class TransferFromThreddsRequest extends SynchronizationRequest {

	private Item targetItem; // can be null
	
	private String remoteFilename;
	
	public TransferFromThreddsRequest(Process process, Item targetItem, FolderItem containingFolder,
			String remoteFilename) {
		super(process,containingFolder);
		this.targetItem = targetItem;
		this.remoteFilename = remoteFilename;
		if(targetItem==null)
			log.debug(String.format("Created IMPORT request of %1$s into %1$s from %3$s",
					remoteFilename,containingFolder.getPath(),containingFolder.getMetadata().getMap().get(Constants.WorkspaceProperties.REMOTE_PATH)));
		else
			log.debug(String.format("Created UPDATE %1$s from %2$s", targetItem.getPath(),
					containingFolder.getMetadata().getMap().get(Constants.WorkspaceProperties.REMOTE_PATH)));
	}
	
	
	
	
}
