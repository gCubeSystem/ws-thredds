package org.gcube.usecases.ws.thredds.faults;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class GenericWebException extends Exception{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5200927893712698886L;

	private String remoteMessage=null;
	
	private Integer responseHTTPCode=0;

	public GenericWebException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GenericWebException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public GenericWebException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GenericWebException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public GenericWebException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	
}
