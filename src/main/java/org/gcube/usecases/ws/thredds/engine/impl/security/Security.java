package org.gcube.usecases.ws.thredds.engine.impl.security;

import org.gcube.common.authorization.client.exceptions.ObjectNotFound;
import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.provider.AuthorizationProvider;
import org.gcube.common.authorization.library.provider.ClientInfo;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.authorization.library.provider.AccessTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.usecases.ws.thredds.model.SynchFolderConfiguration;
import static org.gcube.common.authorization.client.Constants.authorizationService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Security {

	
	
	public static User getCurrent() throws SecurityException {
		String context=ScopeProvider.instance.get();
		if(context==null) throw new SecurityException("Cannot determine context");
		
		log.debug("Context is {}, checking tokens..",context);
		
		ClientInfo client = null;
		try{
			AuthorizationProvider.instance.get().getClient();
		}catch(Throwable  e) {
			log.warn("Unable to get client info ",e);
		}
		User toReturn = new User(client,AccessTokenProvider.instance.get(),SecurityTokenProvider.instance.get(),context);
		
		log.info("Current User is {} ",toReturn);
		return toReturn;
	}
	
	
	public static void set(User toSet) {
		//cleanup everything
		resetUser();
		log.debug("Setting User {} ",toSet);
		if(toSet.getUma_token()!=null)AccessTokenProvider.instance.set(toSet.getUma_token());
		if(toSet.getGcube_token()!=null)SecurityTokenProvider.instance.set(toSet.getGcube_token());
		if(toSet.getContext()!=null)ScopeProvider.instance.set(toSet.getContext());
	}


	public static void resetUser(){
		log.debug("Resetting user");
		SecurityTokenProvider.instance.reset();
		AccessTokenProvider.instance.reset();
		ScopeProvider.instance.reset();
	}
	public static void checkOperator(SynchFolderConfiguration config) throws SecurityException{
		User current=getCurrent();
		log.debug("Checking if current user {} can synch {} ",getCurrent(), config);
		
		// check same vre 
		String expectedContext=config.getTargetContext();
		String currentContext=current.getContext();
		
		if(!expectedContext.equals(currentContext)) 
			throw new SecurityException("Illegal access to folder [root : "+config.getRootFolderId()+", expected context : "+expectedContext+"] from context "+currentContext);
			
		
	}
	
	
	
	public static String getContextFromgcubeToken(String token) throws ObjectNotFound, Exception {
		log.debug("Checking context of gcube-token {}...",token.substring(0,6));
		User caller=getCurrent();		
		try {
			resetUser();
			AuthorizationEntry entry = authorizationService().get(token);
			return entry.getContext();
		}finally {
			log.debug("Resetting user "+caller);
			set(caller);
		}
	}
}
