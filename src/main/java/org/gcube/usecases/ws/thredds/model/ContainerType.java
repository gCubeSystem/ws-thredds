package org.gcube.usecases.ws.thredds.model;

public enum ContainerType {

	FOLDER,
	FILE,
	GENERIC_ITEM,
	URL
	
}
