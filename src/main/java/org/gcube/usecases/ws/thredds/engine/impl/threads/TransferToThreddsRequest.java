package org.gcube.usecases.ws.thredds.engine.impl.threads;

import org.gcube.common.storagehub.model.items.FolderItem;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.usecases.ws.thredds.Constants;
import org.gcube.usecases.ws.thredds.engine.impl.Process;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;


@Getter
@Setter
@Slf4j
public class TransferToThreddsRequest extends SynchronizationRequest {

	private Item toTransfer;

	public TransferToThreddsRequest(Process process,FolderItem location, Item toTransfer) {
		super(process,location);
		this.toTransfer = toTransfer;
		log.debug(String.format("Created SEND request from $1%s to $2%s",
				toTransfer.getPath(),location.getMetadata().getMap().get(Constants.WorkspaceProperties.REMOTE_PATH)));
	}

}
