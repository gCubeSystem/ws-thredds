package org.gcube.usecases.ws.thredds.engine.impl.security;

import org.gcube.common.authorization.library.provider.ClientInfo;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class User{
	private ClientInfo user;
	private String uma_token;
	
	private String gcube_token;
	
	private String context;

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [user=");
		builder.append(user);
		builder.append(", uma_token=");
		builder.append(uma_token==null?uma_token:"***");
		
		builder.append(", gcube_token=");
		builder.append(gcube_token==null?gcube_token:"***");
		
		builder.append(", context=");
		builder.append(context);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}