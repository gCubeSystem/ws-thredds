package org.gcube.usecases.ws.thredds.engine.impl.security;

import org.gcube.usecases.ws.thredds.faults.WorkspaceInteractionException;
import org.gcube.usecases.ws.thredds.model.SynchFolderConfiguration;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SecurityException extends WorkspaceInteractionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -485206587548325970L;
	private User caller;
	private SynchFolderConfiguration folderConfiguration;
	
	public SecurityException() {
		// TODO Auto-generated constructor stub
	}

	public SecurityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public SecurityException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SecurityException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SecurityException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

		
}
