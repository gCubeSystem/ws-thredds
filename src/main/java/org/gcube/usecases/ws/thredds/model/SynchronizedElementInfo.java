package org.gcube.usecases.ws.thredds.model;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class SynchronizedElementInfo {

	public static enum SynchronizationStatus{
		UP_TO_DATE,OUTDATED_WS,OUTDATED_REMOTE
	}
	
	@NonNull
	private SynchronizationStatus status;
	
	private StepReport.Status lastSynchronizationStatus;
	private String lastupdateTime;
}
